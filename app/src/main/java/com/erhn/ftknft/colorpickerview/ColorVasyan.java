package com.erhn.ftknft.colorpickerview;

public final class ColorVasyan
{

    public static ColorVasyan white = new ColorVasyan(255, 255, 255);

    /**
     * <a href=
     * "http://stackoverflow.com/questions/7896280/converting-from-hsv-hsb-in-java-to-rgb-without-using-java-awt-color-disallowe">
     * Converting from HSV (HSB in Java) to RGB without using
     * java.awt.ColorVasyan</a>
     */
    public static int fromHSV(final float hue,
                                final float saturation,
                                final float value)
    {
        final float normaliedHue = (hue - (float) Math.floor(hue));
        final int h = (int) (normaliedHue * 6);
        final float f = normaliedHue * 6 - h;
        final float p = value * (1 - saturation);
        final float q = value * (1 - f * saturation);
        final float t = value * (1 - (1 - f) * saturation);

        switch (h)
        {
            case 0:
                return rgbToString(value, t, p);
            case 1:
                return rgbToString(q, value, p);
            case 2:
                return rgbToString(p, value, t);
            case 3:
                return rgbToString(p, q, value);
            case 4:
                return rgbToString(t, p, value);
            case 5:
                return rgbToString(value, p, q);
            default:
                throw new RuntimeException(String.format(
                        "Could not convert from HSV (%f, %f, %f) to RGB",
                        normaliedHue,
                        saturation,
                        value));
        }
    }


    public static int hsvToRgb(float H, float S, float V) {

        float R, G, B;

        H /= 360f;
        S /= 100f;
        V /= 100f;

        if (S == 0)
        {
            R = V * 255;
            G = V * 255;
            B = V * 255;
        } else {
            float var_h = H * 6;
            if (var_h == 6)
                var_h = 0; // H must be < 1
            int var_i = (int) Math.floor((double) var_h); // Or ... var_i =
            // floor( var_h )
            float var_1 = V * (1 - S);
            float var_2 = V * (1 - S * (var_h - var_i));
            float var_3 = V * (1 - S * (1 - (var_h - var_i)));

            float var_r;
            float var_g;
            float var_b;
            if (var_i == 0) {
                var_r = V;
                var_g = var_3;
                var_b = var_1;
            } else if (var_i == 1) {
                var_r = var_2;
                var_g = V;
                var_b = var_1;
            } else if (var_i == 2) {
                var_r = var_1;
                var_g = V;
                var_b = var_3;
            } else if (var_i == 3) {
                var_r = var_1;
                var_g = var_2;
                var_b = V;
            } else if (var_i == 4) {
                var_r = var_3;
                var_g = var_1;
                var_b = V;
            } else {
                var_r = V;
                var_g = var_1;
                var_b = var_2;
            }

            R = var_r * 255; // RGB results from 0 to 255
            G = var_g * 255;
            B = var_b * 255;
        }


        return android.graphics.Color.rgb((int)R,(int)G,(int)B);
    }


    public static int rgbToString(final float r, final float g, final float b)
    {
        return android.graphics.Color.rgb((int) (r * 255 + 0.5),
                (int) (g * 255 + 0.5),
                (int) (b * 255 + 0.5));
    }

    private final int r, g, b;

    private ColorVasyan(final int r, final int g, final int b)
    {
        if (r < 0)
        {
            this.r = 0;
        }
        else if (r > 255)
        {
            this.r = 255;
        }
        else
        {
            this.r = r;
        }

        if (g < 0)
        {
            this.g = 0;
        }
        else if (g > 255)
        {
            this.g = 255;
        }
        else
        {
            this.g = g;
        }

        if (b < 0)
        {
            this.b = 0;
        }
        else if (b > 255)
        {
            this.b = 255;
        }
        else
        {
            this.b = b;
        }
    }

    @Override
    public String toString()
    {
        final int rgb = (r & 0xFF) << 16 | (g & 0xFF) << 8 | (b & 0xFF) << 0;

        final String htmlColor = "#" + Integer.toHexString(rgb).toUpperCase();
        return htmlColor;
    }

}
