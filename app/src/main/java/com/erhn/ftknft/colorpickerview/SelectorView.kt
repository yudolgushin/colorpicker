package com.erhn.ftknft.colorpickerview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

class SelectorView : View {

    val paint = Paint()

    private var ringColor = Color.WHITE
    var mainColor = Color.BLACK
        set(value) {
            field = value
            invalidate()
        }
    var strokeWidth = 3.dp

    init {
        paint.isAntiAlias = true
        paint.strokeWidth = strokeWidth
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val round = Math.round(56.dp)
        setMeasuredDimension(round, round)
    }

    override fun onDraw(canvas: Canvas) {
        paint.color = mainColor
        paint.style = Paint.Style.FILL
        canvas.drawCircle(width / 2f, height / 2f, width / 2f, paint)
        paint.color = ringColor
        paint.style = Paint.Style.STROKE
        canvas.drawCircle(width / 2f, height / 2f, (width / 2f) - (strokeWidth / 2f), paint)
    }
}