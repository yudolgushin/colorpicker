package com.erhn.ftknft.colorpickerview

import android.content.Context
import android.content.res.Resources
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.FrameLayout

class ColorPickerView : FrameLayout {


    val size = 320
    val array: Array<IntArray> = Array(size) { IntArray(size) }
    val paint = Paint()
    var onColorPick: ((color: Int) -> Unit)? = null
    val dpSize = 1.dp
    val colorCalculator = ColorCalculator()

    constructor(context: Context) : super(context) {
        onInitialize(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        onInitialize(context)
    }

    private fun onInitialize(context: Context) {
        setWillNotDraw(false)
        paint.isAntiAlias = true
        paint.style = Paint.Style.FILL
        paint.strokeWidth = 4.dp
        colorCalculator.calculate(array, size)

    }


    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.x.toInt() >= size || event.y.toInt() >= size) return false
        if (event.x.toInt() < 0 || event.y.toInt() < 0) return false
        val i = array[event.y.toInt()][event.x.toInt()]
        if (i == Color.TRANSPARENT) return false
        onColorPick?.invoke(i)
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val toInt = size.dp.toInt()
        setMeasuredDimension(toInt, toInt)
    }

/*
    fun calculateCoordX(hueRadians: Double, actualRadius: Float, radius: Float): Float {
        val x = (actualRadius * Math.cos(hueRadians) + radius)
        return x.toFloat()

    }

    fun calculateCoordY(hueRadians: Double, actualRadius: Float, radius: Float): Float {
        val y = -(actualRadius * Math.sin(hueRadians) - radius)
        return y.toFloat()

    }*/


    override fun onDraw(canvas: Canvas) {
        paint.style = Paint.Style.FILL
        for (i in 0 until size) {
            for (j in 0 until size) {
                paint.color = array[j][i]
                val fl = i * dpSize
                val fl1 = j * dpSize
                canvas.drawRect(fl, fl1, fl + dpSize, fl1 + dpSize, paint)
            }
        }
        paint.style = Paint.Style.STROKE
        paint.color = Color.WHITE
        canvas.drawCircle(width / 2f, height / 2f, height / 2f, paint)


    }


}


val Int.dp
    get() = Resources.getSystem().displayMetrics.density * this