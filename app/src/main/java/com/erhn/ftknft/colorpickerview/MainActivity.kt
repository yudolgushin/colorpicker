package com.erhn.ftknft.colorpickerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn.setOnClickListener {
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fl_container, FragmentTest())
                .commit()
        }
    }
}
