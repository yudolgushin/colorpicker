package com.erhn.ftknft.colorpickerview

import android.graphics.Color
import java.lang.RuntimeException

class ColorCalculator() {
    fun calculate(array: Array<IntArray>, size: Int) {
        val hsvArray = FloatArray(3)
        for (i in 0 until size) {
            for (j in 0 until size) {
                val saturation = calculateSaturation(j.toFloat(), i.toFloat(), size / 2f)
                val angle = calculateAngle(size / 2f, saturation, j.toFloat(), i.toFloat())
                if (saturation > 1 || saturation == 0f) {
                    if (j == size / 2 && i == size / 2) {
                        array[j][i] = Color.WHITE
                    } else {
                        array[j][i] = Color.TRANSPARENT
                    }

                } else {
                    hsvArray[0] = angle
                    hsvArray[1] = saturation
                    hsvArray[2] = 1f
                    if (j == size / 2 && i == size / 2) {
                        array[j][i] = Color.WHITE
                    } else {
                        array[j][i] = Color.HSVToColor(hsvArray)
                    }
                }
            }
        }
    }


    private fun hsvToRgb(floatArray: FloatArray): Int {
        floatArray[1] = floatArray[1] * 100f
        floatArray[2] = floatArray[2] * 100f
        val h = (floatArray[0] / 60f) % 6f
        val v1 = (100 - floatArray[1]) * floatArray[2] / 100
        val a = (floatArray[2] - v1) * (h % 60) / 60
        val v2 = v1 + a
        val v3 = floatArray[2] - a


        val first = Math.round(floatArray[2] * (2.55f))
        val second = Math.round(v1 * 2.55f)
        val third = Math.round(v2 * 2.55f)
        val toInt = Math.round(v3 * 2.55f)

        return when (h.toInt()) {
            0 -> Color.rgb(first, third, second)
            1 -> Color.rgb(toInt, first, second)
            2 -> Color.rgb(second, first, third)
            3 -> Color.rgb(second, toInt, first)
            4 -> Color.rgb(third, second, first)
            5 -> Color.rgb(first, second, toInt)
            else -> throw RuntimeException("Hue:${floatArray[0]}, saturation:${floatArray[1]}, value:${floatArray[2]}, H:$h")
        }
    }

    private fun calculateSaturation(x: Float, y: Float, radius: Float): Float {
        val actualX = x - radius
        val actualY = radius - y
        val z = Math.sqrt(actualX * actualX + actualY * actualY.toDouble()).toFloat()
        return z / radius
    }

    private fun calculateAngle(radius: Float, saturation: Float, x: Float, y: Float): Float {
        val actualX = x - radius
        val actualY = radius - y
        val fl = radius * saturation
        if (fl == 0f) return 0f
        val cos = actualX / fl
        val cosRad = Math.toDegrees(Math.acos(cos.toDouble()))
        if (actualY < 0) return 360f - cosRad.toFloat()
        return cosRad.toFloat()

    }
}