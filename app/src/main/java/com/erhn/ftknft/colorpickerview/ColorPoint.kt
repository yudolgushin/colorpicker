package com.erhn.ftknft.colorpickerview

class ColorPoint(
    val x: Float,
    val y: Float,
    val hue: Float,
    val saturation: Float,
    val value: Float
)